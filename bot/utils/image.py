from cairosvg.surface import PNGSurface
from pybars import Compiler

class SVGenerator:
    def __init__(self, svg_template):
        """
        :param svg_template: the Mustache SVG template
        :param cache: the cache to use.
        """
        self._svg_template = svg_template

        compiler = Compiler()
        self._template = compiler.compile(self._svg_template.decode('utf-8'))

    def render(self, parameters, scale=1):
        """
        Render the SVG template with passed parameters as PNG
        Will try to read from cache if possible. Will put the render in cache if it was not found.
        :param parameters: the parameters
        :return: the computed PNG as bytestring
        """

        svg = self._template(parameters)

        png_buffer = PNGSurface.convert(bytestring=svg, scale=scale)

        return png_buffer

    def render_to_file(self, parameters, path):
        """
        Same as render, but writes result to file instead of returning bytestring
        """
        with open(path, 'wb') as f:
            f.write(self.render(parameters))

    @staticmethod
    def from_file(path):
        with open(path, 'rb') as f:
            return SVGenerator.from_str(f.read())

    @staticmethod
    def from_str(s):
return SVGenerator(s)