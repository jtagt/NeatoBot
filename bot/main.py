import discord
from discord.ext import commands
import asyncio
import subprocess
import os
import json
import traceback
import requests
from discord.utils import find


def get_prefix(bot, message):
    prefixes = ['..']
    return commands.when_mentioned_or(*prefixes)(bot, message)


cog = [
    'commands.info.stats', 'commands.info.server', 'commands.fun.random',
    'commands.info.user', 'commands.fun.math', 'commands.fun.other',
    'commands.info.setting', 'commands.fun.profile'
]

bot = commands.Bot(
    command_prefix=get_prefix, description='A bot for something.', case_insensitive=True)
bot.remove_command('help')

if __name__ == '__main__':
    for extension in cog:
        bot.load_extension(extension)

bot.run(
    'NDg3NzQ4NjcyNjg4MzU3Mzc3.DnhyxA.v0qcMFpiUqYC0IAdi3ENRlgLfnQ',
    bot=True,
    reconnect=True)
