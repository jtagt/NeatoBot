import discord
from discord.ext import commands
import asyncio
import time
from datetime import datetime
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client.NeatoBot
plugins = db.plugins


class Settings:
    def __init__(self, bot):
        self.bot = bot

    async def on_guild_join(self, guild):
        data = {
            '_id': int(guild.id),
            'help': False,
            'math': False,
            'moderation': False,
            'other': False,
            'random': False,
            'server': False,
            'stats': False,
            'user': False,
            'prefix': ".."
        }
        plugins.insert_one(data)


def setup(bot):
    bot.add_cog(Settings(bot))