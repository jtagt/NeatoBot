import discord
from discord.ext import commands
import asyncio
import time
from datetime import datetime
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client.NeatoBot
plugins = db.plugins

class Server:
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def serverinfo(self, ctx):
        embed = discord.Embed(colour=0xFF5722)
        embed.set_thumbnail(url="https://cdn.discordapp.com/icons/{}/{}.png".format(ctx.guild.id, ctx.guild.icon))
        embed.add_field(name="Server Name", value=ctx.guild.name)
        embed.add_field(name="Server Owner", value=ctx.guild.owner.name + '#' + ctx.guild.owner.discriminator)
        embed.add_field(name="Verification Level", value=str(ctx.guild.verification_level).capitalize())
        embed.add_field(name="Explicit Content Level", value=str(ctx.guild.explicit_content_filter).capitalize())
        embed.add_field(name="MFA Level", value=ctx.guild.mfa_level)
        embed.add_field(name="Splash Picture", value=ctx.guild.splash_url or "None")
        embed.add_field(name="AFK Channel", value=ctx.guild.afk_channel)
        embed.add_field(name="AFK Timeout", value=ctx.guild.afk_timeout)
        embed.add_field(name="Member Count", value=ctx.guild.member_count)
        embed.add_field(name="Role Count", value=len(ctx.guild.roles))
        embed.add_field(name="Emoji Count", value=len(ctx.guild.emojis))
        embed.add_field(name="Voice Region", value=ctx.guild.region)
        embed.add_field(name="Text Channel Count", value=len(ctx.guild.text_channels))
        embed.add_field(name="Voice Channel Count", value=len(ctx.guild.voice_channels))
        embed.add_field(name="Categories Count", value=len(ctx.guild.categories))
        embed.set_footer(text="Guild created at: {}".format(datetime.strftime(ctx.guild.created_at,'%b %d, %Y')))
        await ctx.send(embed=embed)

    @commands.command()
    async def guildemojis(self, ctx):
        emojis = list()
        for emoji in ctx.guild.emojis:
            emojis.append(emoji.name)
        embed = discord.Embed(colour=0xFF5722)
        embed.add_field(name="Emotes list ({})".format(len(ctx.guild.emojis)), value="```{}```".format(', '.join(emojis)))
        await ctx.send(embed=embed)

    @commands.command()
    async def guildroles(self, ctx):
        roles = list()
        for role in ctx.guild.roles:
            roles.append(role.name)
        embed = discord.Embed(colour=0xFF5722)
        embed.add_field(name="Role list ({})".format(len(ctx.guild.roles)), value="```{}```".format(', '.join(roles)))
        await ctx.send(embed=embed)

    @commands.command()
    async def guildchannels(self, ctx):
        text_ch = list()
        voice_ch = list()
        for text in ctx.guild.text_channels:
            text_ch.append(text.name)
        for voice in ctx.guild.voice_channels:
            voice_ch.append(voice.name)
        embed = discord.Embed(colour=0xFF5722)
        embed.add_field(name="Text Channels ({})".format(len(ctx.guild.text_channels)), value="```{}```".format(', '.join(text_ch)))
        embed.add_field(name="Voice Channels ({})".format(len(ctx.guild.voice_channels)), value="```{}```".format(', '.join(voice_ch)))
        await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(Server(bot))