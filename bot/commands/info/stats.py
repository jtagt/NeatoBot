import discord
from discord.ext import commands
import asyncio
import time
from datetime import datetime
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client.NeatoBot
plugins = db.plugins

class Stats:
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def stats(self, ctx):
        embed = discord.Embed(title="Stats", colour=0xFF5722)
        embed.add_field(name="Server Count", value=len(self.bot.guilds))
        await ctx.send(embed=embed)

    @commands.command()
    async def ping(self, ctx):
        t1 = time.perf_counter()
        await ctx.channel.trigger_typing()
        t2 = time.perf_counter()
        pong = f"**Pong!** `{str(round((t2 - t1) * 1000))}ms`"
        embed = discord.Embed(description=pong, color=0xFF5722)
        await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(Stats(bot))