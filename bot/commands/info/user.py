import discord
from discord.ext import commands
import asyncio
import time
from datetime import datetime
from pymongo import MongoClient
import string

client = MongoClient('localhost', 27017)
db = client.NeatoBot
plugins = db.plugins


class User:
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def avatar(self, ctx):
        if '<@' in ctx.message.content:
            uid = ctx.message.mentions[0].id
            member = ctx.guild.get_member(uid)
        else:
            member = ctx.message.author

        embed = discord.Embed(
            title="{}'s Profile".format(member.name), colour=0xFF5722)
        embed.set_image(url=member.avatar_url)
        await ctx.send(embed=embed)

    @commands.command()
    async def userinfo(self, ctx):
        if '<@' in ctx.message.content:
            uid = ctx.message.mentions[0].id
            member = ctx.guild.get_member(uid)
        else:
            member = ctx.message.author

        r = list()
        for roles in member.roles:
            r.append(roles.name)

        embed = discord.Embed(title="User Information: {}".format(member.name), colour=0xFF5722)
        embed.set_thumbnail(url=member.avatar_url)
        embed.add_field(name="Name", value=member.name)
        embed.add_field(name="Status", value=str(member.status).capitalize())
        ca = datetime.strftime(member.created_at,'%b %d, %Y')
        embed.add_field(name="Created account", value=ca)
        js = datetime.strftime(member.joined_at,'%b %d, %Y')
        embed.add_field(name="Joined server", value=js)
        embed.add_field(name="User ID", value=member.id)
        if member.avatar is None:
            n = int(member.discriminator) % 5
            avatar = "https://cdn.discordapp.com/embed/avatars/{}.png".format(n)
            is_animated = 'No'
        else:
            if 'a_' in member.avatar:
                is_animated = 'Yes, very fancy.'
                avatar = "https://cdn.discordapp.com/avatars/{}/{}.gif".format(member.id, member.avatar)
            else:
                is_animated = 'No'
                avatar = "https://cdn.discordapp.com/avatars/{}/{}.png".format(member.id, member.avatar)

        embed.add_field(name="Animated Avatar", value=is_animated)
        embed.add_field(name="Avatar URL", value=avatar)
        embed.add_field(name="Roles", value="```{}```".format(', '.join(r)))
        await ctx.send(embed=embed)

    @commands.command()
    async def id(self, ctx):
        if '<@' in ctx.message.content:
            uid = ctx.message.mentions[0].id
            member = ctx.guild.get_member(uid)
        else:
            member = ctx.message.author
        embed = discord.Embed(
            title="ID: {}".format(member.id), colour=0xFF5722)
        await ctx.send(embed=embed)

    @commands.command()
    async def Help(self, ctx):
        p = plugins.find_one({'_id': int(ctx.guild.id)})
        if p['help'] == True:
            embed = discord.Embed(title="Help", colour=0xFF5722)
            await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(User(bot))