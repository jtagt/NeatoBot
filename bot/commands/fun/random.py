import discord
from discord.ext import commands
import asyncio
import time
from datetime import datetime
import random
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client.NeatoBot
plugins = db.plugins

class Random:
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def random(self, ctx):
        num = random.randint(1,101)
        embed = discord.Embed(title="Your number is: {}".format(num), colour=0xFF5722)
        await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(Random(bot))