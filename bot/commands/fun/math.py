import discord
from discord.ext import commands
import asyncio
import time
from datetime import datetime
import random
import math
from pymongo import MongoClient
import requests
import urllib

client = MongoClient('localhost', 27017)
db = client.NeatoBot
plugins = db.plugins

class Math:
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def math(self, ctx, *, math: str):
        f = { 'expr' : math}
        e = urllib.parse.urlencode(f)
        r = requests.get("http://api.mathjs.org/v4/?{}".format(e))
        a = r.content.decode('utf-8')
        embed = discord.Embed(title="Result: ``{}``".format(a))
        await ctx.send(embed=embed)
def setup(bot):
    bot.add_cog(Math(bot))