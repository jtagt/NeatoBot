import discord
from discord.ext import commands
import asyncio
import time
from datetime import datetime
import random
import requests
from pymongo import MongoClient
from PIL import Image
from io import BytesIO
import urllib

client = MongoClient('localhost', 27017)
db = client.NeatoBot
usr = db.users


class Levels:
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def profile(self, ctx):
        if '<@' in ctx.message.content:
            uid = ctx.message.mentions[0].id
            member = ctx.guild.get_member(uid)
        else:
            member = ctx.message.author

        b = usr.find_one({'_id': int(ctx.message.author.id)})
        if not b:
            data = {
                '_id': int(ctx.message.author.id),
                'background': None,
                'bio': None
            }
            usr.insert_one(data)

        user_id = member.id
        stat = member.status
        username = member.name + "#" + member.discriminator
        avatar = "https://cdn.discordapp.com/avatars/{}/{}.png".format(member.id, member.avatar)

        b = usr.find_one({'_id': int(member.id)})
        if b:
            bio = b['bio']
        else:
            bio = "Put your bio here use ..bio (bio)."
        f = { 'username' : username, 'status': stat, 'bio': bio, 'avatarURL': avatar}
        e = urllib.parse.urlencode(f)
        req = requests.get("https://profile.sdfx.ga/api/users/card.png?{}".format(e))
        data = BytesIO(req.content)

        await ctx.send(file=discord.File(data, filename="card.png"))

    @commands.command()
    async def bio(self, ctx, *, bio: str):
        b = usr.find_one({'_id': int(ctx.message.author.id)})
        if b:
            usr.update_one({'_id': int(ctx.message.author.id)}, {"$set": {"bio": bio}}, upsert=False)
            await ctx.send('Updated')
        else:
            await ctx.send('Looks like you dont have a record yet please run ``profile``.')

def setup(bot):
    bot.add_cog(Levels(bot))