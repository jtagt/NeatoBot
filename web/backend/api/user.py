from flask import Blueprint, g, jsonify, session
from pymongo import MongoClient
import functools
from util.decos import make_session

users = Blueprint('users', __name__, url_prefix='/api/users')
client = MongoClient('localhost', 27017)
db = client.NeatoBot
private_users = db.users
servers = db.servers
plugins = db.plugins

def get_user_managed_servers(user, guilds):
    predicate = list(filter(lambda g: g['owner'] is True or g['permissions'] >> 5 & 1, guilds))
    return predicate


@users.route('/@me')
def users_me():
    if session.get('oauth2_token') is not None:
        discord = make_session(token=session.get('oauth2_token'))
        user = discord.get('https://discordapp.com/api/users/@me').json()
        return jsonify(user)
    else:
        return "", 403

@users.route('/@me/guilds', methods=['GET'])
def guilds():
    if session.get('oauth2_token') is not None:
        discord = make_session(token=session.get('oauth2_token'))
        guilds = discord.get('https://discordapp.com/api/users/@me/guilds').json()
        user = discord.get('https://discordapp.com/api/users/@me').json()
        user_server = get_user_managed_servers(user['id'], guilds)
        return jsonify(user_server)
    else:
        return "", 403