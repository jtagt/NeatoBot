from flask import Blueprint, request, g, jsonify, redirect, render_template
from util.decos import require_auth, get_user
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client.NeatoBot
crd = db.users

cards = Blueprint('cards', __name__, url_prefix='/api/cards')

@cards.route('/update', methods=['POST'])
@require_auth
def update():
    bg = request.headers['background']
    bio = request.headers['bio']
    color = request.headers['color']

    user = get_user()
    
    c = crd.find_one({'_id': int(user['id'])})
    if c:
        crd.update_one({'_id': int(user['id'])}, {'$set': {'color': color or c['color'] or None}})
        crd.update_one({'_id': int(user['id'])}, {'$set': {'background': bg or c['background'] or None}})
        crd.update_one({'_id': int(user['id'])}, {'$set': {'bio': bio or c['bio'] or None}})
        return "Updated", 200
    else:
        return "No Records.", 400

