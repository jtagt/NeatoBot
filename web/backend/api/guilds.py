import yaml
import json
import functools
import operator
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client.NeatoBot
private_users = db.users
servers = db.servers
plugin = db.plugins

from flask import Blueprint, request, g, jsonify, redirect
import functools
from util.decos import require_auth, require_bot_admin

guilds = Blueprint('guilds', __name__, url_prefix='/api/guilds')

def my_dash(f):
    return require_auth(require_bot_admin(f))

def plugin_method(f):
    return my_dash(f)

@guilds.route('/<int:gid>/plugins/enable/<string:pid>', methods=['POST'])
@plugin_method
def enable_plugins(gid, pid):
    check = plugin.find_one({'_id': gid})
    if check[pid] is not None:
        if check[pid] == False:
            plugin.update_one({'_id': gid}, {'$set': {pid: True}})
            return redirect('/dashboard/{}'.format(request.form['guild']))
        else:
            return redirect('/dashboard/{}'.format(request.form['guild']))

@guilds.route('/<int:gid>/plugins/disable/<string:pid>', methods=['POST'])
@plugin_method
def disable_plugins(gid, pid):
    check = plugin.find_one({'_id': gid})
    if check[pid] is not None:
        if check[pid] == False:
            plugin.update_one({'_id': gid}, {'$set': {pid: False}})
            return redirect('/dashboard/{}'.format(request.form['guild']))
        else:
            return redirect('/dashboard/{}'.format(request.form['guild']))

@guilds.route('/<int:gid>/plugins', methods=['GET'])
@plugin_method
def plugins_enabled(gid):
    d = plugin.find_one({'_id': gid})
    if d:
        return jsonify({'help': d['help'], 'random': d['random'], 'stats': d['stats'], 'user': d['user'], 'server': d['server'], 'moderation': d['moderation'], 'math': d['math'], 'other': d['other']})
    else:
        return "Invite the bot.", 403