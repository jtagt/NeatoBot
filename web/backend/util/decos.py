import os
from flask import Flask, g, session, redirect, request, url_for, jsonify
from requests_oauthlib import OAuth2Session
from functools import wraps
import requests

OAUTH2_CLIENT_ID = "487748672688357377"
OAUTH2_CLIENT_SECRET = "UpQtflByDVcdymfvTCMYNXiTEr-ysuPi"
OAUTH2_REDIRECT_URI = 'http://localhost:3000/api/auth/discord/callback'

API_BASE_URL = os.environ.get('API_BASE_URL', 'https://discordapp.com/api')
AUTHORIZATION_BASE_URL = API_BASE_URL + '/oauth2/authorize'
TOKEN_URL = API_BASE_URL + '/oauth2/token'

def token_updater(token):
    session['oauth2_token'] = token

def make_session(token=None, state=None, scope=None):
    return OAuth2Session(
        client_id=OAUTH2_CLIENT_ID,
        token=token,
        state=state,
        scope=scope,
        redirect_uri=OAUTH2_REDIRECT_URI,
        auto_refresh_kwargs={
            'client_id': OAUTH2_CLIENT_ID,
            'client_secret': OAUTH2_CLIENT_SECRET,
        },
        auto_refresh_url=TOKEN_URL,
        token_updater=token_updater)

def get_user():
    discord = make_session(token=session.get('oauth2_token'))
    req = discord.get(API_BASE_URL + '/users/@me')
    user = req.json()
    return user


def get_user_guilds():
    discord = make_session(token=session.get('oauth2_token'))
    req = discord.get(API_BASE_URL + '/users/@me/guilds')
    guilds = req.json()
    return guilds


def require_auth(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        api_token = session['oauth2_state']
        if api_token is None:
            return redirect('/login')
        return f(*args, **kwargs)
    return wrapper

def get_user_managed_servers(user, guilds):
    return list(
        filter(
            lambda g: (g['owner'] is True) or
            bool((int(g['permissions']) >> 5) & 1),
            guilds)
    )
    
ADMINS = ['296084893459283968']
def require_bot_admin(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        server_id = kwargs.get('gid')
        user = get_user()
        if not user:
            return redirect('/api/auth/logout')

        guilds = get_user_guilds()
        user_servers = get_user_managed_servers(user, guilds)
        if user['id'] not in ADMINS and str(server_id) not in map(lambda g: g['id'], user_servers):
            return redirect('/dashboard')

        return f(*args, **kwargs)
    return wrapper