/api/auth/logout:
<pre>
Method: POST
Description: clears the current session
</pre>
    
/api/auth/discord:
<pre>
Method: GET
Description: auths the current user and redirects to /dashboard
</pre>

/api/auth/discord/callback:
<pre>
Method: GET
Description: grabs the state and the token from discord
</pre>
    
/api/guilds/<guild_id>/plugins/enable/<plugin_name>:
<pre>
Method: POST
Description: enables the requested plugin for that guild
</pre>
    
/api/guilds/<guild_id>/plugins/disable/<plugin_name>:
<pre>
Method: POST
Description: disables the requested plugin for that guild
</pre>
    
/api/guilds/<guild_id>/plugins:
<pre>
Method: GET
Description: returns the current guild config
</pre>
```json
{
    "help": false, 
    "math": false, 
    "moderation": false, 
    "other": false, 
    "random": false, 
    "server": false, 
    "stats": false, 
    "user": false
}
```
    
/api/users/@me:
<pre>
Method: GET
Description: returns the authed user's info, if the user isn't authed, then it returns a 403
</pre>
```json
{
    "avatar": "a_adce5f147b61f5530174799397f495d6", 
    "discriminator": "0001", 
    "id": "296084893459283968", 
    "locale": "en-US", 
    "mfa_enabled": false, 
    "username": "Loading User..."
}
```
   
/api/users/@me/guilds:
<pre>
Method: GET
Description: returns the authed user's guilds, if the user isn't authed, then it returns a 403
</pre>
```json
[
    {
        "icon": "d8784615d7342cb634618793760ba440", 
        "id": "350604330673373185", 
        "name": "The Caf\u00e9", 
        "owner": false, 
        "permissions": 2146823415
    }, 
    {
        "icon": "04f73b79c2adf0192f272c5593f56593", 
        "id": "392363146918101006", 
        "name": "Nemo's Nest", 
        "owner": false, 
        "permissions": 2146958847
    }
]
```