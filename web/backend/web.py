import os; os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'

import logging

from flask import Flask, g, session, Blueprint, jsonify, request
import requests

from api.guilds import guilds
from api.user import users
from auth.auth import auth
from api.cards import cards
import json


neato = Flask(__name__)
neato.debug = True

neato.register_blueprint(users)
neato.register_blueprint(auth)
neato.register_blueprint(guilds)
neato.register_blueprint(cards)

DISCORD_CLIENT_ID = "487748672688357377"
OAUTH2_CLIENT_SECRET = "UpQtflByDVcdymfvTCMYNXiTEr-ysuPi"
neato.config['SECRET_KEY'] = OAUTH2_CLIENT_SECRET

@neato.route('/api/plugins', methods=['GET'])
def plugins():
    return jsonify({ 'plugins': [{'name': 'help', 'description': 'Enables the help command'}, 
    {'name': 'random', 'description': 'Enables the random commands'},
    {'name': 'stats', 'description': 'Enables the stats command'},
    {'name': 'user', 'description': 'Enables the user commands'},
    {'name': 'server', 'description': 'Enables the server commands'},
    {'name': 'moderation', 'description': 'Enables the moderation commands'},
    {'name': 'math', 'description': 'Enables the math commands'},
    {'name': 'other', 'description': 'Enables the other commands'}]})

if __name__ == "__main__":
    neato.run(host="0.0.0.0")