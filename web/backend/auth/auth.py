from flask import Blueprint, g, current_app, session, jsonify, redirect, request
from requests_oauthlib import OAuth2Session
from pymongo import MongoClient
import functools
import os
from util.decos import make_session

auth = Blueprint('auth', __name__, url_prefix='/api/auth')

OAUTH2_CLIENT_ID = "487748672688357377"
OAUTH2_CLIENT_SECRET = "UpQtflByDVcdymfvTCMYNXiTEr-ysuPi"
OAUTH2_REDIRECT_URI = 'http://localhost:3000/api/auth/discord/callback'

API_BASE_URL = os.environ.get('API_BASE_URL', 'https://discordapp.com/api')
AUTHORIZATION_BASE_URL = API_BASE_URL + '/oauth2/authorize'
TOKEN_URL = API_BASE_URL + '/oauth2/token'

def token_updater(token):
    pass

@auth.route('/logout', methods=["POST"])
def logout():
    session.clear()
    return redirect('/')


@auth.route('/discord')
def index():
    scope = request.args.get(
        'scope',
        'identify guilds')
    discord = make_session(scope=scope.split(' '))
    authorization_url, state = discord.authorization_url(AUTHORIZATION_BASE_URL)
    session['oauth2_state'] = state
    return redirect(authorization_url)


@auth.route('/discord/callback')
def callback():
    if request.values.get('error'):
        return request.values['error']
    discord = make_session(state=session.get('oauth2_state'))
    token = discord.fetch_token(
        TOKEN_URL,
        client_secret=OAUTH2_CLIENT_SECRET,
        authorization_response=request.url)
    session['oauth2_token'] = token
    return redirect('/dashboard')