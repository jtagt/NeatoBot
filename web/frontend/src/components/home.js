import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Home extends Component {
  render() {
		return (
			<div style={{display: 'flex',  justifyContent:'center', alignItems:'center', height: '100vh'}}>
				<h1> NeatoBot </h1>
				<br></br>
				<Link to="/login">
          <div className="close-support button active">Get Started</div>
        </Link>
			</div>
    );
  }
}

export default Home;
