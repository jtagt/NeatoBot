import React, { Component } from 'react';
import Sidebar from './sidebar';
import {globalState} from '../state';
import {withRouter} from 'react-router';
import { Link } from 'react-router-dom'

class Topbar extends Component {
  constructor() {
    super();

    this.state = {
      guilds: null,
      currentGuildID: globalState.currentGuild ? globalState.currentGuild.id : null,
      showAllGuilds: globalState.showAllGuilds,
    };

    globalState.events.on('showAllGuilds.set', (value) => this.setState({showAllGuilds: value}));

    globalState.getCurrentUser().then((user) => {
      user.getGuilds().then((guilds) => {
        this.setState({guilds});
      });
    });

    globalState.events.on('currentGuild.set', (guild) => {
      this.setState({currentGuildID: guild ? guild.id : null});
    });


    
    globalState.events.on('showAllGuilds.set', (value) => this.setState({showAllGuilds: value}));
  }
  onLogoutClicked() {
    globalState.logout().then(() => {
      this.props.history.push('/login');
    });
  }

  onExpandClicked() {
    globalState.showAllGuilds = !globalState.showAllGuilds;
  }


  render() {
    const expandIcon = this.state.showAllGuilds ? 'fa fa-folder-open-o fa-fw' : ' fa fa-folder-o fa-fw';

		return(
      <div>
        <div className="sub-header window-shadows">
        <Link to="/">
          <div className="close-support button active">Home</div>
        </Link>
        <Link to="/">
          <div className="close-support button active">Support</div>
        </Link>
        <Link to="/api/auth/logout">
          <div className="close-support button active">Logout</div>
        </Link>
        </div>
        <Sidebar />
      </div>
    );
  }
}

export default withRouter(Topbar);
