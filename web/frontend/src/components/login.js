import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'
import {globalState} from '../state';

export default class Login extends Component {
  constructor() {
    super();

    this.state = {
      user: globalState.user,
    };

    globalState.events.on('user.set', (user) => {
      this.setState({user: user});
    });

    globalState.init();
  }

  render() {
    if (this.state.user) {
      return <Redirect to='/' />;
    }

    return (
      <meta http-equiv="refresh" content="0; URL='/api/auth/discord'" />
    );
  }
}
