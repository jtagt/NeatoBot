import React, { Component } from 'react';

import {globalState} from '../state';
import Topbar from './topbar';
import Dashboard from './dashboard';
import Login from './login';
import GuildOverview from './guild_overview';
import CustomCommands from './CustomCommands';
import Home from './home';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';

class AppWrapper extends Component {
  constructor() {
    super();

    this.state = {
      ready: globalState.ready,
      user: globalState.user,
    };

    if (!globalState.ready) {
      globalState.events.on('ready', () => {
        this.setState({
          ready: true,
        });
      });

      globalState.events.on('user.set', (user) => {
        this.setState({
          user: user,
        });
      });

      globalState.init();
    }
  }

  render() {
    const { loading } = this.state;
    
    if(loading) { 
      return null; 
    }

    if (this.state.ready && !this.state.user) {
      return <Redirect to='/login' />;
    }

    return (
      <div id="wrapper">
        <Topbar />
        <div id="page-wrapper">
          <this.props.view params={this.props.params} />
        </div>
      </div>
    );
  }
}

function wrapped(component) {
  function result(props) {
    return <AppWrapper view={component} params={props.match.params} />;
  }
  return result;
}

export default function router() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path='/' component={Home} />
        <Route exact path='/login' component={Login} />
        <Route exact path='/dashboard/:gid' component={wrapped(GuildOverview)} />
        <Route exact path='/dashboard/:gid/commands' component={wrapped(CustomCommands)} />
        <Route exact path='/dashboard' component={wrapped(Dashboard)} />
      </Switch>
    </BrowserRouter>
  );
}
