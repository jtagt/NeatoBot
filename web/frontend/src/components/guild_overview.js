import React, { Component } from 'react';
import {globalState} from '../state';
import {withRouter} from 'react-router';
import { Link } from 'react-router-dom';


class GuildIcon extends Component {
  render() {
    if (this.props.guildIcon) {
      const source = `https://cdn.discordapp.com/icons/${this.props.guildID}/${this.props.guildIcon}.png`;
      return <img src={source} alt="No Icon" />;
    } else {
      return <i>No Icon</i>;
    }
  }
}

class GuildOverviewInfoTable extends Component {
  render() {
    return (
          <section id="dashboardSection">
            <div className="content">
                <div className="PluginsList">
                  <div className="Plugin unlocked">
                      <div className="PluginLeft">
                        <div className="PluginText">
                            <div className="PluginName">Stats</div>
                            <div className="PluginDescription">Neato stats.</div>
                        </div>
                        <form action={"/api/plugins/stats/config/" + this.props.guild.id} method="POST">
                          <div className="PluginActions"><button type="submit" className="button brand disabled small">Enable/Disable</button></div>
                        </form>
                      </div>
                      <div className="PluginRight">
                        <div className="PluginIcon"><img src=""></img></div>
                      </div>
                  </div>
                  <div className="Plugin unlocked">
                      <div className="PluginLeft">
                        <div className="PluginText">
                            <div className="PluginName">Math</div>
                            <div className="PluginDescription">Let NeatoBot do you math homework</div>
                        </div>
                        <form action={"/api/plugins/math/config/" + this.props.guild.id} method="POST">
                          <div className="PluginActions"><button type="submit" className="button brand disabled small">Enable/Disable</button></div>
                        </form>
                      </div>
                      <div className="PluginRight">
                        <div className="PluginIcon"><img src=""></img></div>
                      </div>
                  </div>
                  <div className="Plugin unlocked">
                      <div className="PluginLeft">
                        <div className="PluginText">
                            <div className="PluginName">Server</div>
                            <div className="PluginDescription">Use helpful server commands.</div>
                        </div>
                        <form action={"/api/plugins/server/config/" + this.props.guild.id} method="POST">
                        
                          <div className="PluginActions"><button type="submit" className="button brand disabled small">Enable/Disable</button></div>
                        </form>
                      </div>
                      <div className="PluginRight">
                        <div className="PluginIcon"><img src=""></img></div>
                      </div>
                  </div>
                  <div className="Plugin unlocked">
                      <div className="PluginLeft">
                        <div className="PluginText">
                            <div className="PluginName">Help</div>
                            <div className="PluginDescription">Enables the help command</div>
                        </div>
                        <form action={"/api/plugins/help/config/" + this.props.guild.id} method="POST">
                          <div className="PluginActions"><button type="submit" className="button brand disabled small">Enable/Disable</button></div>
                        </form>
                      </div>
                      <div className="PluginRight">
                        <div className="PluginIcon"><img src=""></img></div>
                      </div>
                  </div>
                </div>
            </div>
          </section>
    );
  }
}

export default class GuildOverview extends Component {
  constructor() {
    super();

    this.state = {
      guild: null,
    };
  }

  ensureGuild() {
    globalState.getGuild(this.props.params.gid).then((guild) => {
      guild.events.on('update', (guild) => this.setState({guild}));
      globalState.currentGuild = guild;
      this.setState({guild});
    }).catch((err) => {
      console.error('Failed to load guild', this.props.params.gid, err);
    });
  }

  componentWillUnmount() {
    globalState.currentGuild = null;
  }

  render() {
    if (!this.state.guild || this.state.guild.id != this.props.params.gid) {
      this.ensureGuild();
      return null; 
    }

    const OverviewTable = withRouter(GuildOverviewInfoTable);

    return (<main>
      <OverviewTable guild={this.state.guild} />
    </main>);
  }
}
