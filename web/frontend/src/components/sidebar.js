import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import {globalState} from '../state';
import {STATS_ENABLED} from 'config'

class GuildLinks extends Component {
  render() {
    let links = [];
    const divStyle = {
      backgroundImage: 'url(' + `https://cdn.discordapp.com/icons/${this.props.guild.id}/${this.props.guild.icon}.png` + ')'
    };
    return (
      <Link to={"/dashboard/" + this.props.guild.id}>
         <div style={divStyle} data-id={this.props.guild.id} tool-tip={this.props.guild.name} className={'server server-header-' + this.props.guild.id}>
        </div>
      </Link>
    );
  }
}


class Sidebar extends Component {
  constructor() {
    super();

    this.state = {
      guilds: null,
      currentGuildID: globalState.currentGuild ? globalState.currentGuild.id : null,
      showAllGuilds: globalState.showAllGuilds,
    };

    globalState.events.on('showAllGuilds.set', (value) => this.setState({showAllGuilds: value}));

    globalState.getCurrentUser().then((user) => {
      user.getGuilds().then((guilds) => {
        this.setState({guilds});
      });
    });

    globalState.events.on('currentGuild.set', (guild) => {
      this.setState({currentGuildID: guild ? guild.id : null});
    });
  }

  render() {
    let sidebarLinks = [];
    if (this.state.guilds) {
      for (let guild of Object.values(this.state.guilds)) {
        if (
          !this.state.showAllGuilds &&
          Object.keys(this.state.guilds).length > 10 &&
          guild.id != this.state.currentGuildID
        ) continue;
        sidebarLinks.push(<GuildLinks guild={guild} active={guild.id == this.state.currentGuildID} key={guild.id} />);
      }
    }

    return (<header className="window-shadows">
    <div className="title">
    <h2 className="">Servers</h2>
    </div>
      <div className="servers">
        {sidebarLinks}
      </div>
    </header>);
  }
}

export default Sidebar;
